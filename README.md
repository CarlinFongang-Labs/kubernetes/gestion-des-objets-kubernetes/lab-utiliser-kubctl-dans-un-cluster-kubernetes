# Lab : Usage de kubectl dans un cluster kubernetes

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______ 

# Objectifs

Kubectl est l'interface principale que la plupart des utilisateurs utilisent pour travailler avec Kubernetes. Ce lab donnera l'occasion de tester et d'affiner notre usage de  **kubectl** avec un cluster Kubernetes. Nous allons : 

1. Obtenir une liste des volumes persistants triés par capacité
2. Exécuter une commande dans un conteneur du pod inclu dans un pod pour obtenir une valeur clé
3. Créer un déploiement à l'aide d'un fichier yaml
4. Supprimer un service grâce à la commande **kubectl delete**


# Pratique 

Nous allons commencé par nous connecter à notre au control plan de notre cluster kubernetes

```bash
ssh -i id_rsa user@public_ip_address_control_node
```

# 1. Lister les volumes persistants triés par capacité

```bash
kubectl get pv
```

>![Alt text](img/image.png)
*Liste de Persistant Volume*

Nous obtenons en entrant cette commande une liste de volume, que nous aimerions classer par taille de volume par exemple. Pour ce faire, nous allons procéder ainsi :

1. Déterminer le nom du champ sur lequel nous souhaitons effectuer le tri. Pour y parvenir nous allons afficher plus en détails les informations de nos PV pour obtenir la valeur clé correspondant à la "capacité" qui est notre critère de tri

```bash
kubectl get pv -o yaml
```
>![Alt text](img/image-1.png)

Je peux identifier dans cette sortie, le segment de propriété ou de spécification qui correspond à la taille du stockage, en l'occurence, il s'agit de : **"spec.capacity.storage"**, présenté sous formation yaml par 

```yaml
spec:
    accessModes:
    - ReadWriteOnce
    capacity:
      storage: 3Gi
```

Une fois identifié la valeur de notre critère de tri, nous pouvons passer la commande suivante pour le resultat escompté :

```bash
kubectl get pv --sort-by=.spec.capacity.storage
```
>![Alt text](img/image-2.png)
*Liste de PV par capacité croissante*

3. Sauvegarde de la liste

Nous allons à présent sauvegarder cette liste trié dans un fichier texte


```bash
kubectl get pv --sort-by=.spec.capacity.storage > /home/cloud_user/pv_list_tri.txt
```

>![Alt text](img/image-3.png)
*Le fichier de sauvegarde à bien été crée*


3. Vérification du conteenu du fichier pv_list_tri.txt

```bash
cat pv_list.txt
```

>![Alt text](img/image-4.png)
*La liste à bien été retranscrite dans le fichier texte*

La sortie à bien été trié par capacité de volume et nous avons pu sauvegarder la liste tel que voulu dans un fichier texte.

# 2. Exécuter un commande dans un conteneur au sein de notre pod

La prochaine étape pour nous est l'exécution d'une commande dans un conteur tournant au sein de notre pod k8s

Pour cela, nous allons commencé par identifier le pod qui nous intéresse, à savoir **"quark"**.

1. Identification du pod

Nous allons donc lister dans un premier temps les namespace et ensuite afficher le contenu du le namespace beebox-mobile 

```bash
kubectl get -namespace
```

>![Alt text](img/image-5.png)
*Liste de namspace sur le cluster k8s*

```bash
kubectl get pods -n beebox-mobile 
```

>![Alt text](img/image-6.png)
*Liste de pods en cours dans le namespace beebox-mobile*

2. Exécutoin d'une commande dans le pod **"quark"**

Dans ce pod, ce trouve en pricipe un fichier dans le repertoire /etc/key/key.txt

Nous allons tout simplement essayer d'afficher le contenu de ce fichier

```bash
kubectl exec quark -n beebox-mobile -- cat /etc/key/key.txt
```

>![Alt text](img/image-7.png)
*Connexion au pod quark réussie*


3. Sauveagerde du fichier **"key.txt"**

Nous allons à présent sauvegarder le fichier contenu dans le pod **quark** sur le repertoire courant de notre control node

```bash
kubectl exec quark -n beebox-mobile -- cat /etc/key/key.txt > /home/cloud_user/key.txt
```

>![Alt text](img/image-8.png)
*Sauverage du fichier contenu dans le pod quark*


4. Créer un déploiement à l'aide d'un fichier de spécifique

Sur notre control node, est présent un fichier de deployment **"deployment.yml"**

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: beebox-mobile
  labels:
    app: beebox-auth
spec:
  replicas: 3
  selector:
    matchLabels:
      app: beebox-auth
  template:
    metadata:
      labels:
        app: beebox-auth
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```

Nous allons exécuter ce deployment en entrant la commande suivante : 

```bash
kubectl apply -f deployment.yml
```

>![Alt text](img/image-9.png)
*Deployment exécuté*

Pour vérifier que le deployment est bien en cours d'exécution, nous allons utiliser un commande permettant d'afficher les deployemnt en y ajoutant comme option, le namespace spécique (présent dans le manifest de deployment) dans le quel il à été deployé 

```bash
kubectl get deployments -n beebox-mobile
```

>![Alt text](img/image-10.png)
*Déploiement du nginx en cours d'exécution*

Nous pouvons également lister l'ensemble des pod en cours d'exécution dans le namespace beebox-mobile

```bash
kubectl get pods -n beebox-mobile
```

>![Alt text](img/image-11.png)
*Liste de pods déployé dans le namespace beebox-mobile opérationnel*

# 5. kubectl delete

Nous pouvons supprimer des objects grace à la commande **kubectl delete**, dans le cas d'un services par exemple, après avoir identifié le service à supprimer : 

```bash
kubectl get services -n beebox-mobile
```

>![Alt text](img/image-12.png)
*Liste de services présent dans le namespace beebox-mobile*

Pour supprimer le service **"beebox-auth-svc"**, nous allons entrer la commande : 

```bash
kubectl delete service beebox-auth-svc -n beebox-mobile
```

>![Alt text](img/image-13.png)
*Le service "beebox-auth-svc" à bien été supprimé*


# Ressources utiles

[Mettre en place un cluster Kubernetes avec kubeadm](https://gitlab.com/CarlinFongang-Labs/kubernetes/lab1.1-install-kubeadm.git)
